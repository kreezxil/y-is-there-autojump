package com.kreezcraft.yisthereautojump;

import net.minecraft.client.Minecraft;
import net.minecraftforge.api.distmarker.Dist;
import net.minecraftforge.fml.common.Mod;
import net.minecraftforge.fml.event.lifecycle.FMLClientSetupEvent;
import net.minecraftforge.fml.javafmlmod.FMLJavaModLoadingContext;
import net.minecraftforge.fml.loading.FMLEnvironment;

import java.io.File;

@Mod("yisthereautojump")
public class YIsThereAutoJump {
    boolean optionsExist = false;

    public YIsThereAutoJump() {
        if (FMLEnvironment.dist == Dist.CLIENT) {
            //File optionsFile = new File(Minecraft.func_71410_x().field_71412_D, "options.txt");

            File optionsFile = new File( "options.txt");

            this.optionsExist = optionsFile.exists();
            FMLJavaModLoadingContext.get().getModEventBus().addListener(this::doClientStuff);
        }

    }

    private void doClientStuff(FMLClientSetupEvent e) {
        if (!this.optionsExist) {
            ((Minecraft)e.getMinecraftSupplier().get()).gameSettings.autoJump = false;
        }

    }

}
